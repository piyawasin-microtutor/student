/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import firebase from 'firebase'
import 'firebase/firestore'
import { Provider } from 'react-redux'
import storeConfig from './src/store/StoreConfig'
import NavigationService, {
  TopLevelNavigator
} from './src/navigation/NavigationService'
import LandingScreen from './src/containers/Landing.container'
import LoginScreen from './src/containers/Login.container'
import ForgetPasswordScreen from './src/containers/ForgetPassword.container'

export default class App extends React.Component {
  componentWillMount() {
    var config = {
      apiKey: 'AIzaSyDIYQF2NpDkI8mSmXO2j34_lebwHpHi-OM',
      authDomain: 'microtutor-test.firebaseapp.com',
      databaseURL: 'https://microtutor-test.firebaseio.com',
      projectId: 'microtutor-test',
      storageBucket: 'microtutor-test.appspot.com',
      messagingSenderId: '442064593667'
    }
    firebase.initializeApp(config)
    const firestore = firebase.firestore()
    firestore.settings({
      timestampsInSnapshots: true
    })
  }
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Provider store={storeConfig().store}>
        <TopLevelNavigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef)
          }}
        />
      </Provider>
    )
  }
}
