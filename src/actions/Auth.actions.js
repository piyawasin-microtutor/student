/* @flow */
import {
  SET_TOKEN,
  CLEAR_TOKEN,
  SET_USER,
  CLEAR_USER
} from '../constants/Auth.constants'

export function setToken(token) {
  return { type: SET_TOKEN, payload: token }
}

export function clearToken() {
  return { type: CLEAR_TOKEN }
}

export function setUser(user) {
  return { type: SET_USER, payload: user }
}

export function clearUser() {
  return { type: CLEAR_USER }
}
