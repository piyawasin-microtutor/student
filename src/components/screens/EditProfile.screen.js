import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text,
  Thumbnail
} from 'native-base'

export default class EditProfileScreen extends React.Component {
  constructor(props) {
    super(props)
    const {
      bio,
      email,
      faculty,
      firstName,
      lastName,
      major,
      photoUrl,
      username,
      year
    } = this.props
    this.state = {
      bio: bio,
      email: email,
      faculty: faculty,
      firstName: firstName,
      lastName: lastName,
      major: major,
      photoUrl: photoUrl,
      username: username,
      year: year
    }
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent danger onPress={this.props.navigateToProfile}>
              <Text>Discard</Text>
            </Button>
          </Left>
          <Body>
            <Title>Edit</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.props.saveProfile}>
              <Text>Save</Text>
            </Button>
          </Right>
        </Header>
        <Content>
          <Form>
            <Thumbnail
              large
              source={{ uri: this.state.photoUrl }}
              style={{
                alignSelf: 'center',
                marginTop: 10,
                marginBottom: 10,
                minWidth: 150,
                minHeight: 150
              }}
            />
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input value={this.state.firstName} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input value={this.state.lastName} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Username</Label>
              <Input value={this.state.username} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input value={this.state.email} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Faculty</Label>
              <Input
                value={this.state.faculty}
                onChangeText={faculty => this.setState({ faculty: faculty })}
                editable={true}
              />
            </Item>
            <Item stackedLabel>
              <Label>Major</Label>
              <Input
                value={this.state.major}
                onChangeText={major => this.setState({ major: major })}
                editable={true}
              />
            </Item>
            <Item stackedLabel>
              <Label>Year</Label>
              <Input
                value={'' + this.state.year}
                onChangeText={year => this.setState({ year: year })}
                editable={true}
              />
            </Item>
            <Item stackedLabel>
              <Label>Bio</Label>
              <Input
                value={this.state.bio}
                onChangeText={bio => this.setState({ bio: bio })}
                editable={true}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
