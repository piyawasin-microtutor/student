/* @flow */
import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text
} from 'native-base'

export default class ForgetPasswordScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
    this.state = { email: '' }
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={this.props.navigateBack}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Reset</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form>
            <Text>Reset password link will be sent via email.</Text>
            <Item stackedLabel last>
              <Label>Email</Label>
              <Input
                autoCapitalize={'none'}
                autoCorrect={false}
                value={this.state.email}
                onChangeText={email => {
                  this.setState({
                    ...this.state,
                    email: email
                  })
                }}
              />
            </Item>
            <Button
              block
              danger
              onPress={() => this.props.reset(this.state.email)}
            >
              <Text>Confirm</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    )
  }
}
