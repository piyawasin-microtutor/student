/* @flow */
import React from 'react'
import {
  Container,
  Button,
  Text,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Title,
  Content
} from 'native-base'

export default class LandingScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Welcome</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Button block primary onPress={this.props.navigateToLogin}>
            <Text>Login</Text>
          </Button>
          <Button block warning onPress={this.props.navigateToRegister}>
            <Text>Register</Text>
          </Button>
        </Content>
      </Container>
    )
  }
}
