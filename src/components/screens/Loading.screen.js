import React from 'react'
import { ActivityIndicator, AsyncStorage, StatusBar, View } from 'react-native'
import { setToken, setUser } from '../../actions/Auth.actions'
import { dispatch } from '../../store/StoreConfig'
import firebase from 'firebase'

export default class LoadingScreen extends React.Component {
  constructor(props) {
    super(props)
    this._bootstrapAsync()
  }

  _saveCredentials = async credentials => {
    try {
      await AsyncStorage.setItem('credentials', JSON.stringify(credentials))
    } catch (error) {
      console.log(error)
    }
  }

  _bootstrapAsync = async () => {
    const asyncItem = await AsyncStorage.getItem('credentials')
    if (asyncItem) {
      const credentials = JSON.parse(asyncItem)
      console.log('asyncitem: ', credentials)
      firebase
        .auth()
        .signInWithEmailAndPassword(credentials.email, credentials.password)
        .then(response => {
          firebase
            .database()
            .ref(`users/students/${response.user.uid}`)
            .once('value')
            .then(snapshot => {
              const user = snapshot.val()
              dispatch(setUser(user))
              this._saveCredentials({
                email: credentials.email,
                password: credentials.password
              })
              this.props.navigation.navigate('App')
            })
        })
    } else {
      this.props.navigation.navigate('Landing')
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    )
  }
}
