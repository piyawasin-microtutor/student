/* @flow */
import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text
} from 'native-base'

export default class LoginScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
    this.state = { email: 'piyawasin@mm.st', password: '12345678' }
    console.log(props)
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={this.props.navigateBack}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Login</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input
                autoCapitalize={'none'}
                autoCorrect={false}
                value={this.state.email}
                onChangeText={email => {
                  this.setState({ ...this.state, email: email })
                }}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Password</Label>
              <Input
                secureTextEntry
                autoCapitalize={'none'}
                autoCorrect={false}
                value={this.state.password}
                onChangeText={password => {
                  this.setState({ ...this.state, password: password })
                }}
              />
            </Item>
            <Button
              block
              primary
              onPress={() =>
                this.props.login(this.state.email, this.state.password)
              }
            >
              <Text>Login</Text>
            </Button>
            <Button
              block
              transparent
              danger
              onPress={this.props.navigateToForgetPassword}
            >
              <Text>Forget password?</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    )
  }
}
