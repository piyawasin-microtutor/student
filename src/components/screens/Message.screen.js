import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text
} from 'native-base'

export default class MessageScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Message</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text>Message</Text>
        </Content>
      </Container>
    )
  }
}
