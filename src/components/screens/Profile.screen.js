import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text,
  Thumbnail
} from 'native-base'

export default class ProfileScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
  }
  render() {
    const {
      bio,
      email,
      faculty,
      firstName,
      lastName,
      major,
      photoUrl,
      username,
      year
    } = this.props
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right>
            <Button transparent block onPress={this.props.navigateToEdit}>
              <Text>Edit</Text>
            </Button>
          </Right>
        </Header>
        <Content>
          <Form>
            <Thumbnail
              large
              source={{ uri: photoUrl }}
              style={{
                alignSelf: 'center',
                marginTop: 10,
                marginBottom: 10,
                minWidth: 150,
                minHeight: 150
              }}
            />
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input value={firstName} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input value={lastName} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Username</Label>
              <Input value={username} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input value={email} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Faculty</Label>
              <Input value={faculty} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Major</Label>
              <Input value={major} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Year</Label>
              <Input value={'' + year} editable={false} />
            </Item>
            <Item stackedLabel>
              <Label>Bio</Label>
              <Input value={bio} editable={false} />
            </Item>
          </Form>
        </Content>
        <Button block transparent danger onPress={this.props.logout}>
          <Text>Logout</Text>
        </Button>
      </Container>
    )
  }
}
