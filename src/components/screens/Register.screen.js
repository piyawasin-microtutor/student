/* @flow */
import React from 'react'
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Content,
  Form,
  Item,
  Title,
  Input,
  Label,
  Button,
  Text
} from 'native-base'

export default class RegisterScreen extends React.Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={this.props.navigateBack}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Register</Title>
          </Body>
          <Right />
        </Header>
        <Content />
      </Container>
    )
  }
}
