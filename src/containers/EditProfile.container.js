import { connect } from 'react-redux'
import firebase from 'firebase'
import NavigationService from '../navigation/NavigationService'
import EditProfileScreen from '../components/screens/EditProfile.screen'

const mapStateToProps = state =>
  ({
    bio,
    email,
    faculty,
    firstName,
    lastName,
    major,
    photoUrl,
    username,
    year
  } = state.auth.currentUser)

const mapDispatchToProps = dispatch => ({
  saveProfile: () => {},
  navigateToProfile: () => {}
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfileScreen)
