import { connect } from 'react-redux'
import firebase from 'firebase'
import NavigationService from '../navigation/NavigationService'
import ForgetPasswordScreen from '../components/screens/ForgetPassword.screen'

const __reset = email => {
  console.log(`Email sent to ${email}.`)
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  reset: email => {
    __reset(email)
  },
  navigateBack: () => {
    NavigationService.navigate('Login')
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgetPasswordScreen)
