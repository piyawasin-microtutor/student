/* @flow */
import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import LandingScreen from '../components/screens/Landing.screen'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  navigateToLogin: () => {
    NavigationService.navigate('Login')
  },
  navigateToRegister: () => {
    NavigationService.navigate('Register')
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingScreen)
