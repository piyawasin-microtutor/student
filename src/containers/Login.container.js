/* @flow */
import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import { AsyncStorage } from 'react-native'
import { dispatch } from '../store/StoreConfig'
import { setUser } from '../actions/Auth.actions'
import firebase from 'firebase'
import LoginScreen from '../components/screens/Login.screen'

_saveCredentials = async credentials => {
  console.log('credentials: ', JSON.stringify(credentials))
  try {
    await AsyncStorage.setItem('credentials', JSON.stringify(credentials))
  } catch (error) {
    console.log(error)
  }
}

const _login = async (email, password) => {
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(response => {
      firebase
        .database()
        .ref(`users/students/${response.user.uid}`)
        .once('value')
        .then(snapshot => {
          const user = snapshot.val()
          dispatch(setUser(user))
          this._saveCredentials({ email: email, password: password })
          NavigationService.navigate('App')
        })
    })
    .catch(error => {
      alert(error)
    })
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  login: (email, password) => {
    _login(email, password)
  },
  navigateToForgetPassword: () => {
    NavigationService.navigate('Forget')
  },
  navigateBack: () => {
    NavigationService.navigate('Landing')
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen)
