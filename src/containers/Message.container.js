/* @flow */
import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import { dispatch } from '../store/StoreConfig'
import firebase from 'firebase'
import MessageScreen from '../components/screens/Message.screen'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageScreen)
