/* @flow */
import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import { dispatch } from '../store/StoreConfig'
import firebase from 'firebase'
import ProfileScreen from '../components/screens/Profile.screen'
import { AsyncStorage } from 'react-native'

const _logout = () => {
  firebase
    .auth()
    .signOut()
    .then(() => {
      try {
        AsyncStorage.removeItem('credentials')
      } catch (error) {
        console.log(error)
      }
      NavigationService.navigate('Landing')
    })
    .catch(error => {
      alert(error)
    })
}

const mapStateToProps = state =>
  ({
    bio,
    email,
    faculty,
    firstName,
    lastName,
    major,
    photoUrl,
    username,
    year
  } = state.auth.currentUser)

const mapDispatchToProps = dispatch => ({
  logout: () => {
    _logout()
  },
  navigateToEdit: () => {
    NavigationService.navigate('Edit')
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen)
