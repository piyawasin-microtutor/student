import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import { dispatch } from '../store/StoreConfig'
import firebase from 'firebase'
import RegisterScreen from '../components/screens/Register.screen'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  navigateBack: () => {
    NavigationService.navigate('Landing')
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterScreen)
