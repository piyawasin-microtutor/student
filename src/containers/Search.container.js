/* @flow */
import { connect } from 'react-redux'
import NavigationService from '../navigation/NavigationService'
import { dispatch } from '../store/StoreConfig'
import firebase from 'firebase'
import SearchScreen from '../components/screens/Search.screen'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchScreen)
