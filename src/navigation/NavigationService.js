import {
  NavigationActions,
  createSwitchNavigator,
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation'
import LandingScreen from '../containers/Landing.container'
import LoginScreen from '../containers/Login.container'
import RegisterScreen from '../containers/Register.container'
import ForgetPasswordScreen from '../containers/ForgetPassword.container'
import ProfileScreen from '../containers/Profile.container'
import MessageScreen from '../containers/Message.container'
import SearchScreen from '../containers/Search.container'
import LoadingScreen from '../containers/Loading.container'
import EditProfileScreen from '../containers/EditProfile.container'

let _navigator

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef
}

function navigate(routeName, params) {
  console.log(routeName)
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  )
}

const AuthStack = createStackNavigator(
  {
    Landing: LandingScreen,
    Login: LoginScreen,
    Register: RegisterScreen,
    Forget: ForgetPasswordScreen
  },
  {
    initialRouteName: 'Landing'
  }
)

const AppStack = createBottomTabNavigator(
  {
    Profile: ProfileScreen,
    Message: MessageScreen,
    Search: SearchScreen,
    Edit: EditProfileScreen
  },
  {
    initialRouteName: 'Profile'
  }
)
export const TopLevelNavigator = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Auth: AuthStack,
    App: AppStack
  },
  {
    initialRouteName: 'Loading'
  }
)

export default {
  navigate,
  setTopLevelNavigator
}
