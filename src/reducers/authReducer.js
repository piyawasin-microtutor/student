/* @flow */
import {
  SET_TOKEN,
  CLEAR_TOKEN,
  SET_USER,
  CLEAR_USER
} from '../constants/Auth.constants'
import { setUser, clearUser } from '../actions/Auth.actions'

const initialState = { currentUser: undefined }

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    // case SET_TOKEN:
    //   return { ...state, firebaseToken: action.payload }
    // case CLEAR_TOKEN:
    //   return { ...state, firebaseToken: undefined }
    case SET_USER:
      return { ...state, currentUser: action.payload }
    case CLEAR_USER:
      return { ...state, currentUser: undefined }
    default:
      return state
  }
}
