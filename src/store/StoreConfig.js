/* @flow */
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import rootReducer from '../reducers/rootReducer'
import thunk from 'redux-thunk'

const middleware = [thunk]
if (__DEV__) {
  middleware.push(createLogger())
}

const store = createStore(
  rootReducer,
  undefined,
  applyMiddleware(...middleware)
)

console.log('store:getState()', store.getState())
console.log('store:dispatch()', store.dispatch)

export const dispatch = store.dispatch
export const getState = store.getState
export default () => {
  return { store }
}
